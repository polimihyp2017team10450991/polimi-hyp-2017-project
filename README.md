# README

This repository is for the project of the Hypermedia Applications' course
of year 2016/2017 relating a clinic's web application.   
This repository has been created by Michele Papale, Giovanni Tommasi and Marco Varrone.

#IMPORTANT: for the examinators
We decided to use Angular4 for the front-end, which uses it's own routing system.    
For that reason, the index.js file that has to be in the root directory,
as described inside the specifications' file, is placed directly inside the folder
'other' that handles the server-side part. Specifically, it's the folder 'other/bin'.
The folder that inside the specifications is called "pages" corresponds here to the folder
"app", for compatibility reasons with Angular.   
The system uses locally (development mode) an SQLite dbms,
while remotely (production mode) uses the PostgreSQL dbms provided by heroku.
To interact with the db, we use KnexJS with the migrations' feature to maintain
the structure of the relations.   
Bootstrap4 has been used as front-end framework, but no template has been used.

#EXTRA form
We decided to develop the reservation form as the EXTRA form requested in the specifications.   
After filling and submitting the form, an email is sent to the specified email address.
The email generally takes 1-2 minutes to arrive.
The reservation is also saved inside the database and it's possible to see the reservations
through the api url /api/reservation.   
Unfortunately Heroku forces a request timeout of 30 seconds. For this reason, sometimes the sending email method may run multiple times, sending more than one copy for the same email.  

#API Documentation
Before every API's method a comment with all the information about it is provided.
To see a textual version of the documentation execute in the command line:   

        apidoc -i other/ -o apidoc

If the command is not found, execute the following command first:

        npm install apidoc -g

An "apidoc" folder will appear and the textual documentation can be opened
with any browser at the path apidoc/index.html.
![alt text](https://bytebucket.org/polimihyp2017team10450991/polimi-hyp-2017-project/raw/4f531706ec60f6f570e37572371484f40dfb2804/public/assets/img/apidoc.PNG?token=ba174b8917b0160ff829d91cab1cd3de3ed2c129)

#Team members' data
Heroku URL: https://polimi-hyp-2017-team-10450991.herokuapp.com/#/   
Bitbucket repo URL: https://bitbucket.org/polimihyp2017team10450991/polimi-hyp-2017-project   
Team administrator: Marco Varrone, 10450991, polimi-hyp-2017-10450991   
Team member n.2: Michele Papale, 1049167, polimi-hyp-2017-1049167   
Team member n.3: Giovanni Tommasi, 10487928, polimi-hyp-2017-10487928

## How do I get set up to test this locally?

-   To run this server locally you should have Nodejs
    (v7.5.0) installed.

-   To install the dependencies:

        npm install

-   To start the server:   

        ng build
        npm run start
