import { json, urlencoded } from "body-parser";
import * as compression from "compression";
import * as express from "express";
import * as path from "path";
let knex = require('./db.js');

import { doctorRouter } from "./routes/doctor";
import { locationRouter } from "./routes/location";
import { serviceRouter } from "./routes/service";
import { areaRouter } from "./routes/area";
import { reservationRouter } from "./routes/reservation";

const app: express.Application = express();

var sg = require('sendgrid')(process.env.SENDGRID_API_KEY);
app.disable("x-powered-by");

app.use(json());
app.use(compression());
app.use(urlencoded({ extended: true }));

/**
 * @api {put} /send/mail Send confirmation email for reservation
 * @apiName SendMail
 * @apiGroup Mail
 *
 * @apiParam {Number} reservation Reservation data
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 202 Accepted
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 401 Unauthorized
 */

let reservation;
app.post('/api/mail', (req, res, next) => {
  sendMail(req.body);
  reservation = req.body;

});

function sendMail(reservation) {
  var helper = require('sendgrid').mail;
  var fromEmail = new helper.Email('app67086929@heroku.com');
  var toEmail = new helper.Email(reservation.email);
  var subject = 'Clinic: reservation confirmation';
  var content = new helper.Content('text/html', `Dear ${reservation.name} ${reservation.surname}, <br>
  You reservation has been sent to our system.
  An operator will call you to confirm it and to give you specific information about the appointment.<br><br>
  These are the following data we received:<br>
  Gender: ${reservation.gender}<br>
  Date of Birth: ${reservation.birthdate}<br>
  Id Card Number: ${reservation.id_card}<br>
  Email address: ${reservation.email}<br>
  Phone: ${reservation.phone}<br>
  Location: ${reservation.location_address}<br>
  Area: ${reservation.area_name}<br>
  Service: ${reservation.service_name}<br>
  Comment: ${reservation.comment != undefined ? reservation.comment : 'None'}<br><br>
  Best regards,<br>
  Clinic
  `);
  var mail = new helper.Mail(fromEmail, subject, toEmail, content);

  var request = sg.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: mail.toJSON()
  });

  sg.API(request, function (error, response) {
    if (error) {
      console.log('Error response received');
    }

    knex('reservation')
    .insert({name: reservation.name,
      surname: reservation.surname,
      gender: reservation.gender,
      birthdate: reservation.birthdate,
      id_card: reservation.id_card,
      email: reservation.email,
      phone: reservation.phone,
      location: reservation.location,
      area: reservation.area,
      service: reservation.service,
      comment: reservation.comment})
      .then((result) => console.log(result));

    console.log(response.statusCode);
    console.log(response.body);
    console.log(response.headers);
  });
}

// api routes
app.use("/api/doctor", doctorRouter);
app.use("/api/location", locationRouter);
app.use("/api/service", serviceRouter);
app.use("/api/area", areaRouter);
app.use("/api/reservation", reservationRouter);

if (app.get("env") === "production") {

  // in production mode run application from dist folder
  app.use(express.static(path.join(__dirname, "/../client")));
}

// catch 404 and forward to error handler
app.use((req: express.Request, res: express.Response, next) => {
  const err = new Error("Not Found");
  next(err);
});

// production error handler
// no stacktrace leaked to user
app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {

  res.status(err.status || 500);
  res.json({
    error: {},
    message: err.message,
  });
});

export { app };
