import { Request, Response, Router } from "express";
let knex = require('../db.js');

const areaRouter: Router = Router();

/**
 * @api {get} /area/ Request all Areas
 * @apiName GetAreas
 * @apiGroup Area
 *
 * @apiSuccess {Number} id Id of the Area.
 * @apiSuccess {String} name Name of the Area.
 * @apiSuccess {Number} responsible Id of the Doctor responsible for this Area.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
      "id": 1,
      "name": "Physiotherapy",
      "responsible": 1
  }, {
      "id": 2,
      "name": "Radiology",
      "responsible": 2
  }
]
 *
 */
areaRouter.get("/", (request: Request, response: Response) => {
  knex.select().from('areas').then((result) => response.json(result));
});

/**
 * @api {get} /area/:id Request Area information
 * @apiName GetArea
 * @apiGroup Area
 *
 * @apiSuccess {Number} id Id of the Area.
 * @apiSuccess {String} name Name of the Area.
 * @apiSuccess {Number} responsible Id of the Doctor responsible for this Area.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
      "id": 1,
      "name": "Physiotherapy",
      "responsible": 1
  }
]
 *
 */
areaRouter.get("/:id", (request: Request, response: Response) => {
  const areaId = request.params.id;
  knex.select().from('areas').where('id', areaId).then((result) => response.json(result));
});

export { areaRouter };
