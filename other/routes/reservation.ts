import { Request, Response, Router } from "express";
let knex = require('../db.js');

const reservationRouter: Router = Router();

/**
 * @api {get} /reservation Request all Reservations
 * @apiName GetReservations
 * @apiGroup Reservation
 *
 * @apiSuccess {Number} id Id of the Reservation.
 * @apiSuccess {String} name Name of the person who made the reservation.
 * @apiSuccess {String} surname Surname of the person who made the reservation.
 * @apiSuccess {String} gender Gender of the person who made the reservation.
 * @apiSuccess {String} id_card Id Card Number of the person who made the reservation.
 * @apiSuccess {String} email Email address of the person who made the reservation.
 * @apiSuccess {String} phone Phone of the person who made the reservation.
 * @apiSuccess {Number} location Location for the reservation.
 * @apiSuccess {Number} area Area for the reservation.
 * @apiSuccess {Number} service Service for the reservation.
 * @apiSuccess {String} comment Comment for the reservation.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
    "id": 2,
    "name": "John",
    "surname": "Doe",
    "gender": "male",
    "birthdate": "1987-10-21",
    "id_card": "5845349",
    "email": "john.doe@mail.com",
    "phone": "555555555",
    "location": 1,
    "area": 1,
    "service": 3,
    "comment": null
  }
]
 *
 */
reservationRouter.get("/", (request: Request, response: Response) => {
  knex.select().from('reservation').then((result) => response.json(result));
});


export { reservationRouter };
