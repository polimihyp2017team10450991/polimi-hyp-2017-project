import { Request, Response, Router } from "express";
let knex = require('../db.js');

const doctorRouter: Router = Router();

/**
 * @api {get} /doctor Request all Doctors in alphabetic order
 * @apiName GetDoctors
 * @apiGroup Doctor
 *
 * @apiSuccess {Number} id Id of the Doctor.
 * @apiSuccess {String} name Name of the Doctor.
 * @apiSuccess {String} surname Surname of the Doctor.
 * @apiSuccess {String} email Email of the Doctor.
 * @apiSuccess {String} degree Degree of the Doctor.
 * @apiSuccess {String} phone Phone of the Doctor.
 * @apiSuccess {String} qualification Qualification of the Doctor.
 * @apiSuccess {String} imageSrc Path for the image of the Doctor.
 * @apiSuccess {String} curriculumVitae CV of the Doctor.
 * @apiSuccess {String} profile Description of the Doctor used in profile page.
 * @apiSuccess {String} treatments Treatments performed by the Doctor.
 * @apiSuccess {Number} service Service performed by the Doctor.
 * @apiSuccess {Number} responsibleOfService Service the Doctor is responsible for.
 * @apiSuccess {Number} responsibleOfArea Area the Doctor is responsible for.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 {
   "id": 7,
   "name": "Bernard",
   "surname": "Chester",
   "degree": "Medicine and Surgery",
   "qualification": "Gastroenterology",
   "phone": "311.358.3119 x3504",
   "email": "bernard.chester@clinic.com",
   "responsibleOfService": 4,
   "service": 4,
   "responsibleOfArea": 1,
   "imageSrc": "doctor-5.jpg",
   "profile": "Dr Chester Bernard is a Consultant in Gastrointestinal and Hepatobiliary Diseases and has been an Endoscopist since 1990...",
   "treatments": "<ul>\n        <li>ERCP</li>\n        <li>Endoscopic Ultrasound</li>",
   "curriculumVitae": "1999 M.D. Loyola Stritch School of Medicine, Maywood, IL..."
}
]
 *
 */
 doctorRouter.get("/", (request: Request, response: Response) => {
  knex.select().from('doctors').orderBy('surname', 'asc').then((result) => response.json(result));
});

/**
 * @api {get} /doctor/location/:id Request Doctors by Location id
 * @apiName GetDoctorByLocation
 * @apiGroup Doctor
 *
 * @apiSuccess {Number} id Id of the Doctor.
 * @apiSuccess {String} name Name of the Doctor.
 * @apiSuccess {String} surname Surname of the Doctor.
 * @apiSuccess {String} email Email of the Doctor.
 * @apiSuccess {String} degree Degree of the Doctor.
 * @apiSuccess {String} phone Phone of the Doctor.
 * @apiSuccess {String} qualification Qualification of the Doctor.
 * @apiSuccess {Number} location Id of the Location where the Doctor works.
 * @apiSuccess {String} imageSrc Path for the image of the Doctor.
 * @apiSuccess {String} curriculumVitae CV of the Doctor.
 * @apiSuccess {String} profile Description of the Doctor used in profile page.
 * @apiSuccess {String} treatments Treatments performed by the Doctor.
 * @apiSuccess {Number} service Id of the Service performed by the Doctor.
 * @apiSuccess {Number} responsibleOfService Id of the Service the Doctor is responsible for.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
    "id": 10,
    "name": "Melinda",
    "surname": "Clark",
    "email": "melinda.clark@clinic.com",
    "degree": "Medicine and Surgery",
    "phone": "(125) 828-4709",
    "qualification": "Dermatology",
    "location": 1,
    "imageSrc": "doctor-10.jpg",
    "curriculumVitae": "2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT...",
    "profile": "Dr Clark is a certified plastic surgeon accredited by the Ministry of Health...",
    "treatments": "Botox: is expected to remain the top cosmetic procedure performed in Orange County...",
    "service": 5,
    "responsibleOfService": 0
  }
]
 *
 */
doctorRouter.get("/location/:id", (request: Request, response: Response) => {
  const locationId = request.params.id;
  knex.select('doctors.id', 'doctors.name', 'doctors.surname', 'doctors.email', 'doctors.degree', 'doctors.phone', 'doctors.qualification', 'locations.id as location', 'doctors.imageSrc', 'doctors.curriculumVitae', 'doctors.profile','doctors.treatments', 'doctors.service', 'doctors.responsibleOfService')
  .from('doctors')
  .innerJoin('services', 'doctors.service', 'services.id')
  .innerJoin('locationService', 'services.id', 'locationService.service')
  .innerJoin('locations', 'locationService.location', 'locations.id')
  .where('locations.id', locationId)
  .orderBy('surname', 'asc')
  .then((result) => response.json(result));
});

/**
 * @api {get} /doctor/area/:id Request Doctors by Area
 * @apiName GetDoctorsByArea
 * @apiGroup Doctor
 *
 * @apiSuccess {Number} id Id of the Doctor.
 * @apiSuccess {String} name Name of the Doctor.
 * @apiSuccess {String} surname Surname of the Doctor.
 * @apiSuccess {String} email Email of the Doctor.
 * @apiSuccess {String} degree Degree of the Doctor.
 * @apiSuccess {String} phone Phone of the Doctor.
 * @apiSuccess {String} qualification Qualification of the Doctor.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
    "id": 5,
    "name": "Vanessa",
    "surname": "Cole",
    "email": "vanessa.cole@clinic.com",
    "degree": "Medicine and Surgery",
    "phone": "(493) 538-6960",
    "qualification": "Rheumatology"
  }
]
 *
 */
doctorRouter.get("/area/:id", (request: Request, response: Response) => {
  const areaId = request.params.id;
  knex.select('doctors.id', 'doctors.name', 'doctors.surname', 'doctors.email', 'doctors.degree', 'doctors.phone', 'doctors.qualification').from('doctors').innerJoin('services', 'doctors.service', 'services.id').innerJoin('areas', 'services.areaOfService', 'areas.id').where('areas.id', areaId)
  .then((result) => response.json(result));
});


/**
 * @api {get} /doctor/service/:id Request Doctors by Service
 * @apiName GetDoctorsByService
 * @apiGroup Doctor
 *
 * @apiSuccess {Number} id Id of the Doctor.
 * @apiSuccess {String} name Name of the Doctor.
 * @apiSuccess {String} surname Surname of the Doctor.
 * @apiSuccess {String} email Email of the Doctor.
 * @apiSuccess {String} degree Degree of the Doctor.
 * @apiSuccess {String} phone Phone of the Doctor.
 * @apiSuccess {String} qualification Qualification of the Doctor.
 * @apiSuccess {String} imageSrc Path for the image of the Doctor.
 * @apiSuccess {String} curriculumVitae CV of the Doctor.
 * @apiSuccess {String} profile Description of the Doctor used in profile page.
 * @apiSuccess {String} treatments Treatments performed by the Doctor.
 * @apiSuccess {Number} service Id of the Service performed by the Doctor.
 * @apiSuccess {Number} responsibleOfService Id of the Service the Doctor is responsible for.
 * @apiSuccess {Number} responsibleOfArea Id of the Area the Doctor is responsible for.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
  {
    "id": 1,
    "name": "John",
    "surname": "Smith",
    "degree": "Medicine and Surgery",
    "qualification": "Radiology",
    "phone": "656.434.0345 x037",
    "email": "john.smith@clinic.com",
    "responsibleOfService": 1,
    "service": 1,
    "responsibleOfArea": 1,
    "imageSrc": "doctor-1.jpg",
    "curriculumVitae":"1994 B.S. University of Notre Dame, Notre Dame, IN...",
    "profile":"Mr. Smith is a qualified Consultant Vascular surgeon...",
    "treatments":"Mr. Smith practices invasive vascular surgery..."
  }
]
 *
 */
doctorRouter.get("/service/:id", (request: Request, response: Response) => {
  const serviceId = request.params.id;
  knex.select()
  .from('doctors')
  .where('service', serviceId)
  .then((result) => response.json(result));
});


/**
 * @api {get} /doctor/:id Request Doctor information
 * @apiName GetDoctor
 * @apiGroup Doctor
 * @apiDescription If the Doctor works in more than one location
 * There will be more than one result with the same values except for the location id
 *
 * @apiSuccess {Number} id Id of the Doctor.
 * @apiSuccess {String} name Name of the Doctor.
 * @apiSuccess {String} surname Surname of the Doctor.
 * @apiSuccess {String} email Email of the Doctor.
 * @apiSuccess {String} degree Degree of the Doctor.
 * @apiSuccess {String} phone Phone of the Doctor.
 * @apiSuccess {String} qualification Qualification of the Doctor.
 * @apiSuccess {Number} location Id of the Location where the Doctor works.
 * @apiSuccess {String} imageSrc Path for the image of the Doctor.
 * @apiSuccess {String} curriculumVitae CV of the Doctor.
 * @apiSuccess {String} profile Description of the Doctor used in profile page.
 * @apiSuccess {String} treatments Treatments performed by the Doctor.
 * @apiSuccess {Number} service Id of the Service performed by the Doctor.
 * @apiSuccess {Number} responsibleOfService Id of the Service the Doctor is responsible for.
 * @apiSuccess {Number} responsibleOfArea Id of the Area the Doctor is responsible for.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
      "id":1,
      "name":"John",
      "surname":"Smith",
      "email":"john.smith@clinic.com",
      "degree":"Medicine and Surgery",
      "phone":"656.434.0345 x037",
      "qualification":"Radiology",
      "location":1,
      "imageSrc":"doctor-1.jpg",
      "curriculumVitae":"1994 B.S. University of Notre Dame, Notre Dame, IN...",
      "profile":"Mr. Smith is a qualified Consultant Vascular surgeon...",
      "treatments":"Mr. Smith practices invasive vascular surgery...",
      "service":1,
      "responsibleOfService":1,
      "responsibleOfArea":1
   }
]
 *
 */
doctorRouter.get("/:id", (request: Request, response: Response) => {
  const doctorId = request.params.id;
  knex.select('doctors.id', 'doctors.name', 'doctors.surname', 'doctors.email', 'doctors.degree', 'doctors.phone', 'doctors.qualification', 'locations.id as location', 'doctors.imageSrc', 'doctors.curriculumVitae', 'doctors.profile','doctors.treatments', 'doctors.service', 'doctors.responsibleOfService')
  .from('doctors')
  .leftJoin('services', 'doctors.service', 'services.id')
  .leftJoin('locationService', 'services.id', 'locationService.service')
  .leftJoin('locations', 'locationService.location', 'locations.id')
  .where('doctors.id', doctorId)
  .then((result) => response.json(result));
});

export { doctorRouter };
