import { Request, Response, Router } from "express";
let knex = require('../db.js');

const locationRouter: Router = Router();

/**
 * @api {get} /location/service/:id Request Location by Service id
 * @apiName GetLocationByService
 * @apiGroup Location
 *
 * @apiSuccess {Number} id Id of the Location.
 * @apiSuccess {String} address Address of the Location.
 * @apiSuccess {Number} imageSrc Path for the image of the Location.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
    "id": 1,
    "address": "Via Tagiura, 10",
    "imageSrc": "location-1.jpg"
  }, {
    "id": 2,
    "address": "Via Francesco Denti, 2",
    "imageSrc": "location-2.jpg"
  }
]
 *
 */
locationRouter.get("/service/:id", (request: Request, response: Response) => {
  const serviceId = request.params.id;
  knex.select('locations.id', 'locations.address', 'imageSrc').from('locations')
  .innerJoin('locationService', 'locations.id', 'locationService.location')
  .where('locationService.service', serviceId)
  .then((result) => response.json(result));
});

/**
 * @api {get} /location Request all Locations
 * @apiName GetLocations
 * @apiGroup Location
 *
 * @apiSuccess {Number} id Id of the Location.
 * @apiSuccess {String} address Address of the Location.
 * @apiSuccess {String} email Email address of the Location.
 * @apiSuccess {String} phone Phone number of the Location.
 * @apiSuccess {String} description Description of the Location.
 * @apiSuccess {Number} imageSrc Path for the image of the Location.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
    "id": 1,
    "address": "Via Tagiura, 10",
    "email": "tagiura@clinic.com",
    "phone": "484.632.5474 x8548",
    "description": "We are situated between the Stadium San Siro and Navigli...",
    "imageSrc": "location-1.jpg"
  }, {
    "id": 2,
    "address": "Via Francesco Denti, 2",
    "email": "f.denti@clinic.com",
    "phone": "761-003-8386 x05279",
    "description": "We are situated near the University \"Politecnico di Milano\"...",
    "imageSrc": "location-2.jpg"
  }
]
 *
 */
locationRouter.get("/", (request: Request, response: Response) => {
  knex.select().from('locations').then((result) => response.json(result));
});

/**
 * @api {get} /location/:id Request Location information
 * @apiName GetLocation
 * @apiGroup Location
 *
 * @apiSuccess {Number} id Id of the Location.
 * @apiSuccess {String} address Address of the Location.
 * @apiSuccess {String} email Email address of the Location.
 * @apiSuccess {String} phone Phone number of the Location.
 * @apiSuccess {String} description Description of the Location.
 * @apiSuccess {Number} imageSrc Path for the image of the Location.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
    "id": 1,
    "address": "Via Tagiura, 10",
    "email": "tagiura@clinic.com",
    "phone": "484.632.5474 x8548",
    "description": "\n          We are situated between the Stadium San Siro and Navigli.<br> The nearest tubes are Gambara and Bande Nere (red tube).<br>\n            If you are travelling by train the nearest station is Milano San Cristoforo.<br>\n            If you are planning to drive please allow yourself time to search for parking and arrange payment.\n            Our clinic operates under a debit/credit park scheme.",
    "imageSrc": "location-1.jpg"
  }
]
 *
 */
locationRouter.get("/:id", (request: Request, response: Response) => {
  const locationId = request.params.id;
  knex.select().from('locations').where('id', locationId).then((result) => response.json(result));
});

/**
 * @api {get} /location/times/:id Request Opening Times of a Location
 * @apiName GetOpeningTimes
 * @apiGroup Location
 *
 * @apiSuccess {Number} id Id of the Location.
 * @apiSuccess {String} weekday Weekday of the Opening Time.
 * @apiSuccess {String} start_time Start time of Opening of the Location.
 * @apiSuccess {String} end_time End time of Opening of the Location.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
    "location_id": 1,
    "weekday": "FRI",
    "start_time": "08:00",
    "end_time": "19:00"
  }, {
    "location_id": 1,
    "weekday": "MON",
    "start_time": "08:00",
    "end_time": "19:00"
  }, {
    "location_id": 1,
    "weekday": "SAT",
    "start_time": "08:00",
    "end_time": "13:00"
  }
]
 *
 */
locationRouter.get("/times/:id", (request: Request, response: Response) => {
  const locationId = request.params.id;
  knex.select().from('opening_times').where('location_id', locationId).then((result) => response.json(result));
});

export { locationRouter };
