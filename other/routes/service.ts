import { Request, Response, Router } from "express";
let knex = require('../db.js');

const serviceRouter: Router = Router();

/**
 * @api {get} /service Request all Services
 * @apiName GetServices
 * @apiGroup Service
 *
 * @apiSuccess {Number} id Id of the Service.
 * @apiSuccess {String} name Name of the Service.
 * @apiSuccess {String} nameBr Name of the Service in HTML format.
 * @apiSuccess {Number} responsibleOfService Id of the Doctor responsible for the Service.
 * @apiSuccess {Number} areaOfService Area of the Service.
 * @apiSuccess {String} description Description of the Service.
 * @apiSuccess {String} imageSrc Path for the image of the Service.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
       "id": 1,
       "name": "Computed axial Tomography",
       "nameBr": "COMPUTED<br>AXIAL<br>TOMOGRAPHY",
       "responsibleOfService": 1,
       "areaOfService": 2,
       "description": "The term <b>\"computed tomography\"</b>, or CT, refers to a computerized x-ray imaging procedure in which a narrow beam of x-rays is...",
       "imageSrc": "service-1.jpg"
   },
   {
    "id": 3,
    "name": "General Rehabilitation",
    "nameBr": "GENERAL<br>REHABILITATION",
    "responsibleOfService": 5,
    "areaOfService": 1,
    "description": "The goal of <b>general rehabilitation</b> is long-term self-management and independence.<br>...",
    "imageSrc": "service-3.jpg"
  }
]
 *
 */
serviceRouter.get("/", (request: Request, response: Response) => {
  knex.select().from('services').then((result) => response.json(result));
});

/**
 * @api {get} /service/:id Request Service information
 * @apiName GetService
 * @apiGroup Service
 *
 * @apiSuccess {Number} id Id of the Service.
 * @apiSuccess {String} name Name of the Service.
 * @apiSuccess {String} nameBr Name of the Service in HTML format.
 * @apiSuccess {Number} responsibleOfService Id of the Doctor responsible for the Service.
 * @apiSuccess {Number} areaOfService Area of the Service.
 * @apiSuccess {String} description Description of the Service.
 * @apiSuccess {String} imageSrc Path for the image of the Service.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
       "id": 1,
       "name": "Computed axial Tomography",
       "nameBr": "COMPUTED<br>AXIAL<br>TOMOGRAPHY",
       "responsibleOfService": 1,
       "areaOfService": 2,
       "description": "The term <b>\"computed tomography\"</b>, or CT, refers to a computerized x-ray imaging procedure in which a narrow beam of x-rays is...",
       "imageSrc": "service-1.jpg"
   }
]
 *
 */
serviceRouter.get("/:id", (request: Request, response: Response) => {
  const serviceId = request.params.id;
  knex.select().from('services').where('id', serviceId).then((result) => response.json(result));
});

/**
 * @api {get} /service/area/:id Request Services by Area
 * @apiName GetServicesByArea
 * @apiGroup Service
 *
 * @apiSuccess {Number} id Id of the Service.
 * @apiSuccess {String} name Name of the Service.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
    "id": 3,
    "name": "General Rehabilitation"
  }, {
    "id": 9,
    "name": "Chronic Pain Management"
  }
]
 *
 */
serviceRouter.get("/area/:id", (request: Request, response: Response) => {
  const areaId = request.params.id;
  knex.select('services.id', 'services.name').from('services').innerJoin('areas', 'services.areaOfService', 'areas.id').where('areaOfService', areaId).then((result) => response.json(result));
});

/**
 * @api {get} /service/location/:id Request Services by Location
 * @apiName GetServicesByLocation
 * @apiGroup Service
 *
 * @apiSuccess {Number} id Id of the Service.
 * @apiSuccess {String} name Name of the Service.
 * @apiSuccess {String} imageSrc Path for the image of the Service.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
   {
    "id": 1,
    "name": "Computed axial Tomography",
    "imageSrc": "service-1.jpg"
  }, {
    "id": 2,
    "name": "Cardiology Echocardiogram",
    "imageSrc": "service-2.jpg"
  }
]
 *
 */
serviceRouter.get("/location/:id", (request: Request, response: Response) => {
  const locationId = request.params.id;
  knex.select('services.id', 'services.name', 'services.imageSrc')
  .from('services')
  .innerJoin('locationService', 'services.id', 'locationService.service')
  .where('locationService.location', locationId)
  .then((result) => response.json(result));
});

export { serviceRouter };
