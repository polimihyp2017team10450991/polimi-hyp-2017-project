var faker = require('faker');
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable("doctors", table => {
      table.increments().primary();
      table.string("name");
      table.string("surname");
      table.string("degree");
      table.string("qualification");
      table.string("phone");
      table.string("email").unique();
      table.integer("responsibleOfService");
      table.integer("service");
      table.integer("responsibleOfArea");
      table.string("imageSrc");
      table.string("profile", 10000);
      table.string("treatments", 10000);
      table.string("curriculumVitae", 10000);
    }).then(() => {
      return knex('doctors').insert([
        {name: 'John', surname: 'Smith', degree: 'Medicine and Surgery', qualification: 'Radiology', phone: faker.phone.phoneNumber(), email: 'john.smith@clinic.com', responsibleOfService: '1', service: '1', responsibleOfArea: '1', imageSrc: 'doctor-1.jpg',
        profile: `Mr. Smith is a qualified Consultant Vascular surgeon and a fellow of Harvard Medical School. He is a consultant to
        Barts and The London NHS Trust running a busy practice in all aspects of vascular surgery. Mr. Smith has a
        particular interest in the field of minimally invasive vascular surgery which he practices at The Private Clinic in
        <b>Via Francesco Denti</b> as well as investigating the role of new technologies and techniques in the management of arterial
        and venous disease.` ,
        treatments: `<b>Treatments</b><br>
        Mr. Smith practices invasive vascular surgery.<br>
        He also practices Computed axial Tomography.`,
        curriculumVitae:`<b>Curriculum Vitae</b><br>
    <b>John Smith</b>, M.D., Ph.D.<br><br>
    <b>Academic Training:</b><br>
    1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
    1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
    2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
    <b>Postdoctoral Training:</b><br>
    1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
    2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
    2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
    2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
    <b>Academic Appointments:</b><br>
    2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
    MD<br>
    2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
    <b>Hospital Appointments:</b><br>
    2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
    2010-present Cardiologist, Boston University Medical Center<br>
    <b>Honors:</b><br>
    1994 Cumlaude graduate, University of Notre Dame<br>
    1994-1999 Navy Health Professions Scholarship Program<br>
    1996 Alpha Omega Alph<br>`
      },
      {name: 'Elisa', surname: 'Parker', degree: 'Medicine and Surgery', qualification: 'Radiology', phone: faker.phone.phoneNumber(), email: 'elisa.parker@clinic.com', responsibleOfService: '0', service: '1', responsibleOfArea: '1', imageSrc: 'doctor-4.jpg',
      profile: `Mrs. Parker is a qualified Consultant Vascular surgeon and a fellow of Harvard Medical School. She is a consultant to
      Barts and The London NHS Trust running a busy practice in all aspects of vascular surgery. Mrs. Parker has a
      particular interest in the field of minimally invasive vascular surgery which he practices at The Private Clinic in
      <b>Via Tagiura</b> as well as investigating the role of new technologies and techniques in the management of arterial
      and venous disease.
      Mrs Parker is a member of a wealth of organisations, namely The Vascular Society of Great Great Britain and
      Ireland, European Society for Vascular Surgery and Association of Surgeons of Great Britain and Ireland. She has
      performed over 1500 EVLA Endo Venous Laser Ablation the most successful minimally invasive laser treatments
      for the removal of varicose veins.
      Mrs. Parker has also pioneered the use of EVLA as a treatment for veins on the hands and he is one of very
      few surgeons in Europe trained in this revolutionary technique. Mrs. Parker is also experienced in performing
      varicose vein treatments to the scrotum and varicose vein treatments to the vulva and vagina, often as treatment
      for pelvic congestion.` ,
      treatments: `<b>Treatments</b><br>
      Mrs. Parker practices radiation therapy for cancer and Computed axial Tomography.<br>
      She is the responsible of Radiology in our <b>Clinic</b>.`,
      curriculumVitae:`<b>Curriculum Vitae</b><br>
  <b>Elisa Parker</b>, M.D., Ph.D.<br><br>
  <b>Academic Training:</b><br>
  1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
  1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
  2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
  <b>Postdoctoral Training:</b><br>
  1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
  2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
  2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
  2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
  <b>Academic Appointments:</b><br>
  2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
  MD<br>
  2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
  <b>Hospital Appointments:</b><br>
  2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
  2010-present Cardiologist, Boston University Medical Center<br>
  <b>Honors:</b><br>
  1994 Cumlaude graduate, University of Notre Dame<br>
  1994-1999 Navy Health Professions Scholarship Program<br>
  1996 Alpha Omega Alph<br>`
      },
      {name: 'Hyum', surname: 'Kim', degree: 'Medicine and Surgery', qualification: 'Cardiology', phone: faker.phone.phoneNumber(), email: 'hyum.kim@clinic.com', responsibleOfService: '2', service: '2', responsibleOfArea: '1', imageSrc: 'doctor-2', imageSrc: 'doctor-2.jpg',
      profile: `Dr Hyum graduated with MBBChir from Cambridge University, England.<br>
       He obtained a First Class Honours in Medical Sciences Tripos while he was an undergraduate at Cambridge.
      He is Fellow of the Royal Colleges of Physicians of London and Edinburgh as well as Fellow of the American Colleges of Cardiology, Medicine and American Heart Association.<br>
      He is American board certified (Diplomate) in both Cardiology and Internal Medicine.
      Dr Hyum has been practising cardiology for the last 35 years. He trained in cardiology at the Hammersmith Hospital London, Cedars-Sinai Medical Centre/UCLA in Los Angeles and has worked for 15 years in Los Angeles as a clinical and interventional cardiologist prior to returning to Singapore in 1992 as an Associate Professor of Medicine/Cardiology at the National University Hospital (NUH).<br>
     He was Acting Head of the Department of Cardiology at <b>Via Francesco Denti Clinic</b> prior to his entry into private practice in 1996.` ,
      treatments: `<b>Treatments</b><br>
      Mr. Hyum practices heart surgery.<br>
      In particular:<br>
      <ul>
        <li>Ablation</li>
        <li>Angioplasty & Stenting</li>
        <li>Cardiac Rehabilitation</li>
        <li>Coronary Artery Bypass Grafting</li>
        <li>Heart Transplant</li>
        <li>Heart Valve Repair</li>
      </ul>`,
      curriculumVitae:`<b>Curriculum Vitae</b><br>
  <b>Hyum Kim</b>, M.D., Ph.D.<br><br>
  <b>Academic Training:</b><br>
  1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
  1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
  2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
  <b>Postdoctoral Training:</b><br>
  1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
  2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
  2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
  2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
  <b>Academic Appointments:</b><br>
  2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
  MD<br>
  2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
  <b>Hospital Appointments:</b><br>
  2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
  2010-present Cardiologist, Boston University Medical Center<br>
  <b>Honors:</b><br>
  1994 Cumlaude graduate, University of Notre Dame<br>
  1994-1999 Navy Health Professions Scholarship Program<br>
  1996 Alpha Omega Alph<br>`
      },
      {name: 'Giovanna', surname: 'Mandrelli', degree: 'Medicine and Surgery', qualification: 'Cardiology', phone: faker.phone.phoneNumber(), email: 'giovanna.mandrelli@clinic.com', responsibleOfService: '0', service: '2', responsibleOfArea: '1', imageSrc: 'doctor-3.jpg',
      profile: `Dr Mandrelli is a cardiologist with more than 30 years of experience, and currently practises out of his clinics at Mount Elizabeth and Mount Elizabeth Novena Hospitals.<br>
       She specialises in caring for patients through interventional cardiology and implantations of stents.<br>
       Upon his graduation (MBBS, 1966), Dr Mandrelli attained his post-graduate degrees (MRCGP UK, 1975, MRCP Ireland, 1977 and MRCP UK, 1979).<br>
       She then underwent Cardiology Advanced Specialty Training in Singapore and England and was inducted as a Fellow of the Academy of Medicine Singapore (Cardiology) in 1981.<br>
      She also became a Fellow of the Royal College of Physicians (Edinburgh), Fellow of the Royal College of Physicians (Ireland) and Fellow of the Royal College of Physicians (London).<br>
       In the years following she was admitted as a Fellow of the Royal College of General Practitioners (UK), Fellow of the American College of Chest Physicians (USA), Fellow of the American College of Cardiology (USA) and Fellow European Society of Cardiology (Europe). She then went on to train in interventional cardiology at the Regional Cardio-Thoracic Centre in England, the San Francisco Heart Institute in USA, the Cleveland Clinic in USA and the Broussais University Hospital in France.<br>
A respected figure in the cardiology circle, Dr Mandrelli has addressed many regional and international cardiology conventions including the World Congress of Cardiology. She also gives regular lectures to doctors and nurses in Singapore and in Asia.<br> The new concept of cardio cerebral resuscitation and laughter aerobics are close to his heart and she frequently gives talks and demonstrations to the medical community and the public on these concepts.
The well-published cardiologist is also active in other arenas and is kept busy with social, cultural, educational, sports and voluntary activities. Serving as the Charter President of the Rotary Club of Serangoon Garden Orchard, Singapore from 1990 to 1991 was one such responsibility that he took on wholeheartedly. For his dedication to the community, she was awarded the Pingat Bakti Masyarakat (PBM) from the Government of Singapore.` ,
      treatments:`<b>Treatments</b><br>
      Mrs. Mandrelli practices heart surgery.<br>
      In particular:<br>
      <ul>
        <li>Heart Valve Replacement</li>
        <li>Implantable Defibrillator</li>
        <li>Pacemakers</li>
        <li>Patent Foramen Ovale Repair</li>
        <li>Transcatheter Aortic Valve Replacement (TAVR)</li>
        <li>Ventricular Assist Device</li>
      </ul>` ,
      curriculumVitae:`<b>Curriculum Vitae</b><br>
  <b>Giovanna Mandrelli</b>, M.D., Ph.D.<br><br>
  <b>Academic Training:</b><br>
  1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
  1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
  2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
  <b>Postdoctoral Training:</b><br>
  1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
  2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
  2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
  2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
  <b>Academic Appointments:</b><br>
  2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
  MD<br>
  2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
  <b>Hospital Appointments:</b><br>
  2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
  2010-present Cardiologist, Boston University Medical Center<br>
  <b>Honors:</b><br>
  1994 Cumlaude graduate, University of Notre Dame<br>
  1994-1999 Navy Health Professions Scholarship Program<br>
  1996 Alpha Omega Alph<br>`
      },
      {name: 'Vanessa', surname: 'Cole', degree: 'Medicine and Surgery', qualification: 'Rheumatology', phone: faker.phone.phoneNumber(), email: 'vanessa.cole@clinic.com', responsibleOfService: '3', service: '3', responsibleOfArea: '1', imageSrc: 'doctor-7.jpg',
      profile: `Trained in Rheumatology in the department of Rheumatology, Hammersmith Hospital, London, UK More than 25 Years experience as a Rheumatologist.<br>
      Previously Head of Department of Rheumatology in Tan Tock Seng Hospital and Division of Rheumatology, National University Hospital (NUH).<br>
      Currently, she is an adjunct associate professor in the Department of Medicine, National University Hospital (NUSH).` ,
      treatments: `<b>Treatments</b><br>
      <ul>
        <li><b>EMG (electromyography)</b>: inserting fine needle electrodes in muscles and observing the recorded motor unit potentials when the muscles are activated to help distinguish whether weakness is due to muscle or nerve dysfunction (i.e., myopathy vs. neuropathy).</li>

        <li><b>NCS (nerve conduction studies)</b>: use of electrodes to record motor and sensory responses that are propagated by electrical stimuli. This test can help distinguish location of a nervous system lesion (radiculopathy, peripheral neuropathy, motor neuron disease, or neuromuscular junction).</b>

        <li><b>Peripheral joint injections</b>: injections to help diagnose and treat bone and soft tissue disorders often seen in orthopedic, rheumatologic, and sports medicine disorders such as knee osteoarthritis, rotator cuff tendinopathy, and epicondylitis.</b>

        <li><b>Trigger point injections</b>: lidocaine or dry needling can be used as an adjunct to proper exercise and physical therapy to treat trigger points, thought to be sources of chronic myofascial (soft-tissue) pain.</li>
      </ul>`,
      curriculumVitae:`<b>Curriculum Vitae</b><br>
  <b>Vanessa Cole</b>, M.D., Ph.D.<br><br>
  <b>Academic Training:</b><br>
  1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
  1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
  2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
  <b>Postdoctoral Training:</b><br>
  1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
  2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
  2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
  2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
  <b>Academic Appointments:</b><br>
  2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
  MD<br>
  2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
  <b>Hospital Appointments:</b><br>
  2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
  2010-present Cardiologist, Boston University Medical Center<br>
  <b>Honors:</b><br>
  1994 Cumlaude graduate, University of Notre Dame<br>
  1994-1999 Navy Health Professions Scholarship Program<br>
  1996 Alpha Omega Alph<br>`
      },
      {name: 'Arnold', surname: 'Sullivan', degree: 'Medicine and Surgery', qualification: 'Neurology', phone: faker.phone.phoneNumber(), email: 'arnold.sullivan@clinic.com', responsibleOfService: '0', service: '3', responsibleOfArea: '1', imageSrc: 'doctor-6.jpg',
      profile: `Primary/Secondary Education: 1959-1969 St. Anthony's School, Perak, Malaysia<br>
        Pre-University Education: 1970-1971 National Junior College, Singapore<br>
        Undergraduate Education: 1972-1977 University of Singapore<br>
        Degrees/Diplomas: 1977 M.B.B.S. University of Singapore,<br>
         1983 M. Med (Int Med) National University of Singapore,<br>
          1984 MRCP (Ireland) Royal college of Physicians, Dublin, 1994 FRCP (Ireland) Royal College of Physicians, Dublin, 1994 Corresponding Fellow American Academy of Neurology` ,
      treatments: `<b>Treatments</b><br>
      <ul>
        <li><b>Musculoskeletal ultrasound</b>: although it has been used for decades as a modality to deliver deep heat in therapies, ultrasound is now increasingly being used in the outpatient setting to supplement the musculoskeletal evaluation. Ultrasound may be used to evaluate for soft tissue abnormalities in commonly examined joints and structures. This technology is also now frequently used to guide injections, as it allows for improved placement of needles for delivery of treatment without exposure to ionizing radiation.</li>

        <li><b>Spasticity management</b>: spasticity is a common complication related to CNS injury (e.g., SCI, stroke, cerebral palsy). Physiatrists treat spasticity by using oral antispasticity agents, botulinum toxin injections, phenol injections, and intrathecal baclofen pump management to improve function and decrease pain.</li>

        <li><b>Interventional spinal therapeutics</b>: image-guided spinal diagnostics and injections, including discograms, interlaminar and transforaminal epidurals, and radiofrequency ablations, spinal cord stimulation, vertebroplasty/kyphoplasty, and intrathecal pump placements. These techniques are being used as a nonsurgical pain-relieving intervention for back pain and radiculopathy.</li>
    </ul>`,
      curriculumVitae:`<b>Curriculum Vitae</b><br>
  <b>Arnold Sullivan</b>, M.D., Ph.D.<br><br>
  <b>Academic Training:</b><br>
  1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
  1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
  2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
  <b>Postdoctoral Training:</b><br>
  1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
  2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
  2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
  2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
  <b>Academic Appointments:</b><br>
  2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
  MD<br>
  2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
  <b>Hospital Appointments:</b><br>
  2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
  2010-present Cardiologist, Boston University Medical Center<br>
  <b>Honors:</b><br>
  1994 Cumlaude graduate, University of Notre Dame<br>
  1994-1999 Navy Health Professions Scholarship Program<br>
  1996 Alpha Omega Alph<br>`
      },
      {name: 'Bernard', surname: 'Chester', degree: 'Medicine and Surgery', qualification: 'Gastroenterology', phone: faker.phone.phoneNumber(), email: 'bernard.chester@clinic.com', responsibleOfService: '4', service: '4', responsibleOfArea: '1', imageSrc: 'doctor-5.jpg',
      profile: `Dr Chester Bernard is a Consultant in Gastrointestinal and Hepatobiliary Diseases and has been an Endoscopist since 1990.<br> Previously with Singapore General Hospital's (SGH) Department of Gastroenterology and Hepatology, he entered private practice in Mount Elizabeth Medical Centre in 2000.<br>
      In 1983, Dr Chester graduated among the top 6 in his medical class with an MBBS from the National University of Singapore and was awarded the Mickie Prize for Surgery at the Final MBBS exam.<br>
       In 1988 he obtained his Masters Degree in Internal Medicine and also became a Member of the Royal College of Physicians (United Kingdom).<br>
        He was admitted as a Fellow of the Academy of Medicine (Singapore) in 1999 and chose to undergo advanced training in Toronto Canada from 1991-1992, with special emphasis on Inflammatory Bowel Disease (Crohn's Disease, Ulcerative Colitis), bowel failure (short bowel syndrome , malabsorption), and Total Parenteral Nutrition (TPN).<br>
         His work has seeded the establishment of specialist clinics for such diseases in SGH, in addition to overseeing, formalising and training the multidisciplinary team running the hospital wide TPN service of SGH from 1993-2000.<br>
          He was awarded the Harry M Vars Research Award for a paper on TPN related osteoporosis, a piece of research work completed while he was overseas .` ,
      treatments: `<b>Treatments</b><br>
      <ul>
        <li>ERCP</li>
        <li>Endoscopic Ultrasound</li>
        <li>Liver Biopsy</li>
        <li>Video capsule endoscopy</li>
      </ul>`,
      curriculumVitae:`<b>Curriculum Vitae</b><br>
  <b>Bernard Chester</b>, M.D., Ph.D.<br><br>
  <b>Academic Training:</b><br>
  1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
  1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
  2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
  <b>Postdoctoral Training:</b><br>
  1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
  2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
  2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
  2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
  <b>Academic Appointments:</b><br>
  2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
  MD<br>
  2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
  <b>Hospital Appointments:</b><br>
  2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
  2010-present Cardiologist, Boston University Medical Center<br>
  <b>Honors:</b><br>
  1994 Cumlaude graduate, University of Notre Dame<br>
  1994-1999 Navy Health Professions Scholarship Program<br>
  1996 Alpha Omega Alph<br>`
      },
      {name: 'Francesca', surname: 'Sandrelli', degree: 'Medicine and Surgery', qualification: 'Gastroenterology', phone: faker.phone.phoneNumber(), email: 'francesca.sandrelli@clinic.com', responsibleOfService: '0', service: '4', responsibleOfArea: '1', imageSrc: 'doctor-8.jpg',
      profile: `In 1995 Dr Francesca Sandrelli joined the National University Hospital (NUH) and National University of Singapore (NUS) as a PhD scholar pursuing a Doctorate in Medical Studies and subsequently completed a PhD thesis on Gastric Cancer in 2000.<br> During her time at NUH, she was awarded The Young Doctor's Research Award at NUH's Inaugural Annual Scientific Meeting in 1997 and the Best Paper Award for oral presentation at the Nitric Oxide Symposium in 1998.<br>
       In 2009 she won the Best Oral Presentation Award (Medical Disciplines) at the National Healthcare Group's 8th Annual Scientific Congress.<br>
       In 2000 Dr Sandrelli obtained her Master of Internal Medicine, Singapore (MMed) and in 2001 her Membership of the Royal Colleges of Physicians of the UK (MRCP).<br>
        In 2005 she became part of the Fellowship of the Academy of Singapore in Gastroenterology (FAMS).<br>
        In 2008 Dr Sandrelli was awarded a Health Manpower Development Plan (HMDP) Fellowship by the Ministry of Health, Singapore to further her training in ERCP and Therapeutic Endoscopy at the European Endoscopic Training Center at the Universita Cattolica Del Sacro Cuore, Gemelli Hospital in Rome, Italy under Professor Guido Costamagna, President of the European Society of Gastrointestinal Endoscopy.<br>
        As a Consultant at the Department of Gastroenterology and Hepatology, Dr Sandrelli practiced at the National University Hospital in Singapore until April 2010.<br>
         She enjoyed teaching the undergraduate students at the Yong Loo Lin School of Medicine as a clinical tutor and was appointed chief coordinator for MRCP PACES in 2009. In addition to that, she was a mentor and endoscopy trainer for advanced specialists and endoscopy trainees.<br>
         Dr Sandrelli received several awards for Clinical Services: In 2007, she received the Sunny-Side-Up NUH Service Award, the Sunny-Side-Up Wow NUH Service Award and the Excellent Service GOLD Award. In 2008, she received the National Excellent Service EXSA Star Award.
         In April 2010 Dr Francesca Sandrelli entered private practice and set up her clinic at Via Francesco Denti.` ,
      treatments: `<b>Treatments</b><br>
      <ul>
        <li><b>ERCP</b></li>
        <li><b>Gastroscopy</b>: to inspect the inside of the stomach, gullet and the duodenum</li>
        <li><b>Colonoscopy</b>: to inspect the large and small intestines</li>
        <li><b>Flexible Sigmoidoscopy</b></li>
      </ul>`,
      curriculumVitae:`<b>Curriculum Vitae</b><br>
  <b>Francesca Sandrelli</b>, M.D., Ph.D.<br><br>
  <b>Academic Training:</b><br>
  1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
  1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
  2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
  <b>Postdoctoral Training:</b><br>
  1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
  2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
  2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
  2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
  <b>Academic Appointments:</b><br>
  2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
  MD<br>
  2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
  <b>Hospital Appointments:</b><br>
  2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
  2010-present Cardiologist, Boston University Medical Center<br>
  <b>Honors:</b><br>
  1994 Cumlaude graduate, University of Notre Dame<br>
  1994-1999 Navy Health Professions Scholarship Program<br>
  1996 Alpha Omega Alph<br>`
      },
      {name: 'Bob', surname: 'Jackerson', degree: 'Medicine and Surgery', qualification: 'Plastic surgery', phone: faker.phone.phoneNumber(), email: 'bob.jackerson@clinic.com', responsibleOfService: '5', service: '5', responsibleOfArea: '1', imageSrc: 'doctor-9.jpg',
      profile: `Dr Bob is a Ministry of Health accredited consultant Plastic Surgeon. He graduated from Medical School at the National University of Singapore.<br> Thereafter he completed his Plastic Surgery training at the Singapore General Hospital.<br> He was awarded the College of Surgeons Gold Medal for being the top candidate in his cohort.<br> He completed a one year Fellowship at the University of Washington, Seattle, where he trained under the world –renowned Prof Peter Neligan. <br>
      It was there that he further honed his skills in reconstructive microsurgery and cosmetic surgery.<br>
       Upon his return, he served as a Consultant Plastic Surgeon at Singapore General Hospital as well as Visiting Consultant at Khoo Teck Puat Hospital.<br>
        Dr Jackerson is well versed in the latest techniques of cosmetic surgery of the face as well as non-invasive facial rejuvenation.<br>
         He is regularly invited to speak at various local and international medical conferences where he trains and shares his expertise on aesthetic plastic and reconstructive surgery to fellow doctors.<br>
          He is also an expert on Botulinum toxin and filler injection and has given live demonstrations to fellow doctors as well as conducted cadaveric dissections and demonstrations on filler injections.<br>
           Body contouring is also one of Dr Leo’s main interests. He has one of the largest series of Body Contouring Post Massive Weight Loss patients in Singapore.<br>
            Dr Jackerson has also presented extensively on this subject both at regional as well as international conferences.<br>
             Dr Jackerson is also actively involved within the plastic surgery fraternity. He is the Secretary for both the Singapore Association of Plastic Surgeons as well as the Singapore Society of Cosmetic (Aesthetic) Surgeons for the year 2014-2015.<br>
              Dr Jackerson is also actively involved in the academic training and development of budding young surgeons. He is an Adjunct Assistant Professor with the Duke-NUS Graduate Medical School. In addition, he was also an Examiner for the Final M.B.B.S Surgical Examination and an Observer Examiner for the plastic surgery exit examinations.<br>
               Dr Jackerson also participates regularly in humanitarian mission work. He has been to China, Indonesia and Vietnam on numerous occasions to perform cleft lip and palate surgeries on these underprivileged patients.` ,
      treatments: `<b>Treatments</b><br>
      <ul>
        <li><b>Botox</b>: is expected to remain the top cosmetic procedure performed in Orange County. Botox is well established in Orange County and is a quick, effective, and safe procedure to help remove lines and wrinkles created by muscle activity.  Although similar products, such as Xeomin and Dysport, provide similar effects, Botox remains the top seller in this category.

          The most popular treatment areas continue to be the wrinkles in-between the eyebrows, often called the “elevens”, and the second most popular is the “crow’s feet” area; that is, the wrinkles on the sides of the eyes.  Many patients not only like the effect of smoothing the crow’s feet but Botox placed in this area by an experienced injector can (counterintuitively) elevate the tail of the eyebrows, providing a beautiful aesthetic effect to the eyes.

          Other areas that have gained in popularity, and showing no signs of decreasing in 2015, is the treatment of lip lines and neck bands with Botox.  For those hearing about topical Botox or something similar, so far no product has been FDA approved, maybe in 2 to 3 years.</li>
        <li><b>Voluma</b> has been on the market in the United States and Orange County for over one year now and has already gained a reputation of providing excellent and long lasting results.  The trade name Juvederm-Voluma, is a third generation filler produced by Allergan, the same company behind Botox and Latisse (the eyelash grower).

        Voluma is designed to correct the aging related volume loss that develops in the cheeks (midface) and lower face.  It continues to be the only one that is FDA approved for these areas.  When Voluma is used to correct the volume loss fully, the results can last up to two years!  And as an added safety feature the results of Voluma are reversible.</li>
        <li><b>Eyelid rejuvenation</b>:

        Eyelid rejuvenation, also known as blepharoplasty (bleph-a-row-plas-tea), was one of the most popular procedures performed in the United States and Orange County in 2014, and based on current estimates will be the leading facial aesthetic surgical procedure in 2015, along with fractional CO2 laser skin resurfacing and facelifts.

        The popularity for this procedure is increasing as the population increases in age.</li>
      </ul>`,
      curriculumVitae:`<b>Curriculum Vitae</b><br>
  <b>Bob Jackerson</b>, M.D., Ph.D.<br><br>
  <b>Academic Training:</b><br>
  1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
  1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
  2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
  <b>Postdoctoral Training:</b><br>
  1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
  2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
  2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
  2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
  <b>Academic Appointments:</b><br>
  2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
  MD<br>
  2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
  <b>Hospital Appointments:</b><br>
  2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
  2010-present Cardiologist, Boston University Medical Center<br>
  <b>Honors:</b><br>
  1994 Cumlaude graduate, University of Notre Dame<br>
  1994-1999 Navy Health Professions Scholarship Program<br>
  1996 Alpha Omega Alph<br>`
      },
      {name: 'Melinda', surname: 'Clark', degree: 'Medicine and Surgery', qualification: 'Dermatology', phone: faker.phone.phoneNumber(), email: 'melinda.clark@clinic.com', responsibleOfService: '0', service: '5', responsibleOfArea: '1', imageSrc: 'doctor-10.jpg',
      profile: `Dr Clark is a certified plastic surgeon accredited by the Ministry of Health.<br>
        As a plastic surgeon, Dr Clark is proud to be part of a fraternity noted for their thoughtful and methodical approach to problem solving, as well as their fine and delicate touch.<br>
        Dr Clark had the privilege of a broad based training covering all aspects of reconstruction for congenital or acquired problems, wound management and aesthetic surgery during his advanced surgical training in plastic surgery, both in Singapore and abroad.<br> She was awarded a one year scholarship by the Human Manpower Development Program (Ministry of Health) to further his training at The Nottingham Breast Institute in his two special areas of interests – microsurgery and breast reconstruction. There, she gained invaluable experience in perforator flap free tissue transfers and oncoplastic breast surgery.<br>
        She is one of a handful of surgeons who offer all possible options in breast reconstruction, be it post breast removal or post breast conservation.<br>
        As a consultant plastic surgeon at the KK Hospital, Singapore's premiere hospital for women and children, Dr Clark acquired the added experience and special skills in reconstructive surgery for children, gynaecological reconstruction, lymphoedema surgery and post-natal rejuvenation (mummy makeover).<br>
        She is best described as a young plastic surgeon who believes in old values - she sees himself as first a doctor, then as a plastic surgeon. <br>
        She is committed to provide the best possible care for his patients.<br>
        Clinical focus: Breast aesthetic and reconstructive surgery, paediatric plastic surgery, gynaecological plastic surgery, scar treatment, lymphoedema treatment` ,
      treatments: `<b>Treatments</b><br>
      <ul>
        <li><b>Botox</b>: is expected to remain the top cosmetic procedure performed in Orange County. Botox is well established in Orange County and is a quick, effective, and safe procedure to help remove lines and wrinkles created by muscle activity.  Although similar products, such as Xeomin and Dysport, provide similar effects, Botox remains the top seller in this category.

          The most popular treatment areas continue to be the wrinkles in-between the eyebrows, often called the “elevens”, and the second most popular is the “crow’s feet” area; that is, the wrinkles on the sides of the eyes.  Many patients not only like the effect of smoothing the crow’s feet but Botox placed in this area by an experienced injector can (counterintuitively) elevate the tail of the eyebrows, providing a beautiful aesthetic effect to the eyes.

          Other areas that have gained in popularity, and showing no signs of decreasing in 2015, is the treatment of lip lines and neck bands with Botox.  For those hearing about topical Botox or something similar, so far no product has been FDA approved, maybe in 2 to 3 years.</li>
        <li><b>HydraFacial</b>:

          The HydraFacial is a multiple award winning facial that is quickly becoming the “go-to” facial for those wanting to have their skin looking its best for that special day or celebration, or simply wanting to have a facial that truly imparts nutrition and health to the skin.

          The HydraFacial is a common feature in popular magazines (NewBeauty, Elle, Harpers, InStyle, Brides, etc.) and is often on popular TV shows (The Doctors, Real Housewives of Beverly Hills, Good day LA, etc.).  The HydraFacial has been shown to improve the appearance of the skin better than microdermabrasion and IPL (photofacial).  Additionally, the extraction process is less traumatic and painful than typically performed during regular facials.</li>
        <li><b>Ultherapy</b>:

In 2014, the non-surgical treatment Ultherapy had a tremendous upsurge in popularity after it received yet another FDA approval, this time for improving lines and wrinkles of the décolletage (chest).  No other treatment is FDA approved for this hard to treat area.  Previous FDA indications for Ultherapy are for lifting the eyebrows, lower face, and neck, including underneath the chin.

In 2015, Ultherapy will continue to grow in popularity because patients are seeking non-surgical treatments that deliver real results.  For those looking for a non-surgical lift, Ultherapy may be the best treatment for yourself in 2015.</li>
      </ul>`,
      curriculumVitae:`<b>Curriculum Vitae</b><br>
  <b>Melinda Clark</b>, M.D., Ph.D.<br><br>
  <b>Academic Training:</b><br>
  1994 B.S. University of Notre Dame, Notre Dame, IN; Cum Laude, Preprofessional Studies<br>
  1999 M.D. Loyola Stritch School of Medicine, Maywood, IL<br>
  2008 Ph.D. Yale Graduate School of Arts and Sciences, New Haven, CT; Investigative Medicine<br><br>
  <b>Postdoctoral Training:</b><br>
  1999-2000 Intern, Yale-New Haven Hospital, New Haven, CT<br>
  2002-2003 Resident, Internal Medicine, Yale-New Haven Hospital, New Haven, CT<br>
  2003-2006 Research Fellow in Cardiology, Mentor Larry Young, Yale School of Medicine, New Haven, CT<br>
  2006-2008 Clinical Fellow in Cardiology, Yale-New Haven Hospital, New Haven, CT<br><br>
  <b>Academic Appointments:</b><br>
  2008-2010 Assistant Professor of Medicine (adjunct), Uniformed Services University of the Health Sciences, Bethesda,
  MD<br>
  2010-present Assistant Professor of Medicine and Radiology, Boston University School of Medicine<br><br>
  <b>Hospital Appointments:</b><br>
  2008-2010 Staff Cardiologist, National Naval Medical Center, Bethesda, MD<br>
  2010-present Cardiologist, Boston University Medical Center<br>
  <b>Honors:</b><br>
  1994 Cumlaude graduate, University of Notre Dame<br>
  1994-1999 Navy Health Professions Scholarship Program<br>
  1996 Alpha Omega Alph<br>`
      }
      ]);
    }),
  ]);

};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('doctors');
};
