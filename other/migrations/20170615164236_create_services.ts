exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema.createTable("services", table => {
        table.increments().primary();
        table.string("name");
        table.string("nameBr");
        table.integer("responsibleOfService");
        table.integer("areaOfService");
        table.string("description", 10000);
        table.string("imageSrc");
      }).then(() => {
        return knex("services").insert([
            {name: "Computed axial Tomography", nameBr: "COMPUTED<br>AXIAL<br>TOMOGRAPHY", responsibleOfService: '1', areaOfService: '2',
            description: `  <b>What it's for</b><br><br>
            The term <b>"computed tomography"</b>, or CT, refers to a computerized x-ray imaging procedure in which a narrow beam of x-rays is
            aimed at a patient and quickly rotated around the body, producing signals that are processed by the machine's computer to
            generate cross-sectional images - or "slices"- of the body.<br> These slices are called tomographic images and contain more detailed
            information than conventional x-rays. Once a number of successive slices are collected by the machine's computer, they can be
            digitally "stacked" together to form a three-dimensional image of the patient that allows for easier identification and location of basic
            structures as well as possible tumors or abnormalities.<br><br>

            <b>How it's done</b><br>
            In many ways CT scanning works very much like other x-ray examinations. Different body parts absorb the x-rays in varying
            degrees. It is this crucial difference in absorption that allows the body parts to be distinguished from one another on an x-ray film or
            CT electronic image.<br>
            In a conventional x-ray exam, a small amount of radiation is aimed at and passes through the part of the body being examined,
            recording an image on a special electronic image recording plate. Bones appear white on the x-ray; soft tissue, such as organs like
            the heart or liver, shows up in shades of gray, and air appears black.<br>
            With CT scanning, numerous x-ray beams and a set of electronic x-ray detectors rotate around you, measuring the amount of
            radiation being absorbed throughout your body. Sometimes, the examination table will move during the scan, so that the x-ray beam
            follows a spiral path. A special computer program processes this large volume of data to create two-dimensional cross-sectional
            images of your body, which are then displayed on a monitor.`, imageSrc:"service-1.jpg"
          },
          {name: "Cardiology Echocardiogram", nameBr: "CARDIOLOGY<br>ECHOCARDIOGRAM", responsibleOfService: '3', areaOfService: '3',
            description: `
            <b>What it's for</b><br>
            An <b>echocardiogram</b>, often referred to as a cardiac echo or simply an echo, is a sonogram of the heart.<br>
            (It is not abbreviated as ECG, because that is an abbreviation for an electrocardiogram).<br>
             Echocardiography uses standard two-dimensional, three-dimensional, and Doppler ultrasound to create images of the heart.<br><br>

          Echocardiography has become routinely used in the diagnosis, management, and follow-up of patients with any suspected or known heart diseases.
          It is one of the most widely used diagnostic tests in cardiology.<br>
          It can provide a wealth of helpful information, including the size and shape of the heart (internal chamber size quantification), pumping capacity, and the location and extent of any tissue damage.
           An echocardiogram can also give physicians other estimates of heart function, such as a calculation of the cardiac output, ejection fraction, and diastolic function (how well the heart relaxes).<br><br>

          Echocardiography can help detect cardiomyopathies, such as hypertrophic cardiomyopathy, dilated cardiomyopathy, and many others.<br>
          The use of stress echocardiography may also help determine whether any chest pain or associated symptoms are related to heart disease.<br>
           The biggest advantage to echocardiography is that it is not invasive (does not involve breaking the skin or entering body cavities) and has no known risks or side effects.`,
           imageSrc:"service-2.jpg"
          },
          {name: "General Rehabilitation", nameBr: "GENERAL<br>REHABILITATION", responsibleOfService: '5', areaOfService: '1',
            description:`
            The goal of <b>general rehabilitation</b> is long-term self-management and independence.<br>
            Treatment plans address mobility, self-care, cognitive abilities (memory, understanding and communication deficits) and endurance for maximum participation in healthy daily living.

            Through general rehabilitation, patients, including those who are not candidates for independent living, can achieve an enhanced quality of life.<br>
            Individual programs may also be developed for patients whose recovery from injury or illness may be complicated by unrelated existing medical conditions, frailty or advanced age.
            `, imageSrc:"service-3.jpg"
          },
          {name: "General Appendicitis", nameBr: "GENERAL<br>APPENDICITIS", responsibleOfService: '7', areaOfService: '5',
            description:`
            <b>Appendicitis</b> is inflammation of the appendix.<br>
            Symptoms commonly include right lower abdominal pain, nausea, vomiting, and decreased appetite.
            However, approximately 40% of people do not have these typical symptoms.
            Severe complications of a ruptured appendix include widespread, painful inflammation of the inner lining of the abdominal wall and sepsis.<br><br>

            Appendicitis is caused by a blockage of the hollow portion of the appendix.
             This is most commonly due to a calcified "stone" made of feces.<br>
             Inflamed lymphoid tissue from a viral infection, parasites, gallstone, or tumors may also cause the blockage.
              This blockage leads to increased pressures in the appendix, decreased blood flow to the tissues of the appendix, and bacterial growth inside the appendix causing inflammation.
              The combination of inflammation, reduced blood flow to the appendix and distention of the appendix causes tissue injury and tissue death.
              If this process is left untreated, the appendix may burst, releasing bacteria into the abdominal cavity, leading to increased complications.<br><br>

              The diagnosis of appendicitis is largely based on the person's signs and symptoms.<br>
              In cases where the diagnosis is unclear, close observation, medical imaging, and laboratory tests can be helpful.<br>
             The two most common imaging tests used are an ultrasound and computed tomography (CT scan).<br>
            CT scan has been shown to be more accurate than ultrasound in detecting acute appendicitis.<br>
            However, ultrasound may be preferred as the first imaging test in children and pregnant women because of the risks associated with radiation exposure from CT scans.<br><br>

            The standard treatment for acute appendicitis is surgical removal of the appendix.<br>
            This may be done by an open incision in the abdomen (laparotomy) or through a few smaller incisions with the help of cameras (laparoscopy).<br>
            Surgery decreases the risk of side effects or death associated with rupture of the appendix.<br>
            Antibiotics may be equally effective in certain cases of non-ruptured appendicitis.`, imageSrc:"service-4.jpg"
          },
          {name: "Aesthetics Facelift", nameBr: "AESTHETICS<br>FACELIFT", responsibleOfService: '9', areaOfService: '4',
            description:`
            There are a variety of types of <b>facial rejuvenation treatments and procedures</b>.
            While non-surgical methods can be incredibly effective, in many cases, surgical methods prove to be the better approach and may produce better results.
            If your face has begun to show signs of aging such as sagging skin, fine lines, wrinkles, and folds, the following facial rejuvenation surgeries can help you achieve a more refreshed and youthful appearance.<br><br>


            <b>Facelift Surgery</b><br>
            Facelift surgery can improve many signs of facial aging in the mid face, lower face, and even the neck.
            Various techniques including the mini-facelift, mid-face lift, deep plane facelift, neck liposuction, and Ultherapy can correct the following issues:

            <ul>
              <li>Jowls</li>
              <li>Sagging facial tissue</li>
              <li>Turkey neck</li>
              <li>Descending cheeks</li>
              <li>Nasolabial folds</li>
              <li>Wrinkles and lines in the mid and lower face</li>
            </ul>

            <b>Silhouette Lift</b><br>
            The Silhouette Lift is a minimally invasive surgical procedure that can lift the deeper layers of the facial skin to rejuvenate your appearance.
            The surgeon uses sutures with bidirectional cones to elevate and reposition the skin on your face.
            Over time, your body naturally absorbs the sutures and stimulates more collagen to improve skin quality.

            <b>Brow Lift Surgery</b>
            Brow lift surgery can lift and tighten the tissues of the upper face.
            This surgical approach corrects horizontal forehead lines, drooping brows, and brow furrows, and it may also improve upper eyelid hooding in some cases.
            Certain techniques can also elevate sagging cheeks.
            Patients who need a brow lift may feel that they look continually angry, sad, or tired.
            After the procedure, your forehead and brow area will appear lifted and smooth, erasing the negative expression that previously marred your appearance.

            <b>Blepharoplasty</b>
            Another facial rejuvenation surgery is blepharoplasty, more commonly called eyelid surgery.
            This procedure can correct sagging eyelids and displaced fat in the areas around the eyes. Patients can receive upper blepharoplasty, lower blepharoplasty, or both to fully rejuvenate the tissues around the eyes.
            The surgeon will adjust the muscle tissue, eliminate excess skin and upper eyelid hooding that may impair vision, soften fine lines, redistribute fat tissue, and reduce puffiness and bags around the eyes.`,
            imageSrc:"service-5.jpg"
          },
          {name: "Coronary Intervention", responsibleOfService: '0', areaOfService: '3',
            description:``
          },
          {name: "Cardiac Imaging", responsibleOfService: '0', areaOfService: '3',
            description:``
          },
          {name: "Stress Echocardiography", responsibleOfService: '0', areaOfService: '3',
            description:``
          },
          {name: "Chronic Pain Management", responsibleOfService: '0', areaOfService: '1',
            description:``
          },
          {name: "Orthopedic Rehabilitation", responsibleOfService: '0', areaOfService: '1',
            description:``
          },
          {name: "Amputation Rehabilitation", responsibleOfService: '0', areaOfService: '1',
            description:``
          },
          {name: "Stroke Rehabilitation", responsibleOfService: '0', areaOfService: '1',
            description:``
          },
          {name: "Neurorehabilitation", responsibleOfService: '0', areaOfService: '1',
            description:``
          },
          {name: "Mammography", responsibleOfService: '0', areaOfService: '2',
            description:``
          },
          {name: "Intervention Radiology", responsibleOfService: '0', areaOfService: '2',
            description:``
          },
          {name: "MRI", responsibleOfService: '0', areaOfService: '2',
            description:``
          },
          {name: "Fluoroscopy", responsibleOfService: '0', areaOfService: '2',
            description:``
          },
        ]);
      }),
    ]);
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('services');
};
