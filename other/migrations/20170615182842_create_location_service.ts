exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema.createTable("locationService", table => {
        table.increments().primary()
        table.integer("location");
        table.integer("service");
      }).then(() => {
        return knex("locationService").insert([
          {location: '1', service: '1'},
          {location: '1', service: '2'},
          {location: '1', service: '3'},
          {location: '1', service: '5'},
          {location: '2', service: '1'},
          {location: '2', service: '3'},
          {location: '2', service: '4'},
          {location: '3', service: '2'},
          {location: '3', service: '3'},
          {location: '3', service: '5'}
          ]);
      }),
    ]);
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('locationService');
};
