exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('opening_times', table => {
      table.bigInteger('location_id').unsigned().references('id').inTable('locations');
      table.enu('weekday', ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']);
      table.time('start_time');
      table.time('end_time');
      table.primary(['location_id', 'weekday']);

    }).then(() => {
      return knex('opening_times').insert([
        {location_id: '1', weekday: 'MON', start_time: '08:00', end_time: '19:00'},
        {location_id: '1', weekday: 'TUE', start_time: '08:00', end_time: '13:00'},
        {location_id: '1', weekday: 'WED', start_time: '08:00', end_time: '19:00'},
        {location_id: '1', weekday: 'THU', start_time: '08:00', end_time: '13:00'},
        {location_id: '1', weekday: 'FRI', start_time: '08:00', end_time: '19:00'},
        {location_id: '1', weekday: 'SAT', start_time: '08:00', end_time: '13:00'},
        {location_id: '2', weekday: 'MON', start_time: '08:30', end_time: '19:30'},
        {location_id: '2', weekday: 'TUE', start_time: '08:30', end_time: '13:30'},
        {location_id: '2', weekday: 'WED', start_time: '08:30', end_time: '19:30'},
        {location_id: '2', weekday: 'THU', start_time: '08:30', end_time: '13:30'},
        {location_id: '2', weekday: 'FRI', start_time: '08:30', end_time: '19:30'},
        {location_id: '2', weekday: 'SAT', start_time: '08:30', end_time: '13:30'},
        {location_id: '3', weekday: 'MON', start_time: '08:00', end_time: '19:00'},
        {location_id: '3', weekday: 'TUE', start_time: '08:00', end_time: '13:00'},
        {location_id: '3', weekday: 'WED', start_time: '08:00', end_time: '19:00'},
        {location_id: '3', weekday: 'THU', start_time: '08:00', end_time: '13:00'},
        {location_id: '3', weekday: 'FRI', start_time: '08:00', end_time: '19:00'},
        {location_id: '3', weekday: 'SAT', start_time: '08:00', end_time: '13:00'}
      ]);
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('opening_times');
};
