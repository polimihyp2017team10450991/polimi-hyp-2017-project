var faker = require('faker');

exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema.createTable("locations", table => {
        table.increments().primary();
        table.string("address");
        table.string("email");
        table.string("phone");
        table.text("description");
        table.string("imageSrc");
      }).then(() => {
        return knex("locations").insert([
          {address: 'Via Tagiura, 10', email: 'tagiura@clinic.com', phone: faker.phone.phoneNumber(), description: `
          We are situated between the Stadium San Siro and Navigli.<br> The nearest tubes are Gambara and Bande Nere (red tube).<br>
            If you are travelling by train the nearest station is Milano San Cristoforo.<br>
            If you are planning to drive please allow yourself time to search for parking and arrange payment.
            Our clinic operates under a debit/credit park scheme.`, imageSrc:"location-1.jpg"},
          {address: 'Via Francesco Denti, 2', email: 'f.denti@clinic.com', phone: faker.phone.phoneNumber(),
          description: `We are situated near the University "Politecnico di Milano".<br> The nearest tubes are Piola and Lambrate (green tube).<br>
            If you are travelling by train the nearest station is Lambrate.<br>
            If you are planning to drive please allow yourself time to search for parking and arrange payment.
            Our clinic operates under a debit/credit park scheme.`, imageSrc:"location-2.jpg"},
          {address: 'Via Cipro, 5', email: 'cipro@clinic.com', phone: faker.phone.phoneNumber(), description: `
          We are situated near the Parco Vittorio Formentano.<br> The nearest tube is Piola (green tube).<br>
            If you are travelling by train the nearest station is Milano Dateo.<br>
            If you are planning to drive please allow yourself time to search for parking and arrange payment.
            Our clinic operates under a gratis park scheme.`, imageSrc:"location-3.jpg"},
        ]);
      }),
    ]);
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('locations');
};
