exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema.createTable("areas", table => {
        table.increments().primary();
        table.string("name")
        table.integer("responsible");
      }).then(() => {
        return knex("areas").insert([
          {name: 'Physiotherapy', responsible: '1'},
          {name: 'Radiology', responsible: '2'},
          {name: 'Cardiology', responsible: '3'}
        ]);
      }),
    ]);
};

exports.down = function(knex, Promise) {
      return knex.schema.dropTable('areas');
};
