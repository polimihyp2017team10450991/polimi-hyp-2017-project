exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema.createTable("reservation", table => {
        table.increments().primary();
        table.string('name');
        table.string('surname');
        table.string('gender');
        table.string('birthdate');
        table.string('id_card');
        table.string('email');
        table.string('phone');
        table.integer('location');
        table.integer('area');
        table.integer('service');
        table.string('comment');
      })
    ]);
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('reservation');
};
