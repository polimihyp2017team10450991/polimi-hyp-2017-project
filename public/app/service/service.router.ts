import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ServiceComponent } from './service.component';
import { AllServicesComponent } from './all-services.component';

const routes: Route[] = [
  { path: '',  component: AllServicesComponent },
  { path: 'location/:id',  component: AllServicesComponent },
  { path: ':id', component: ServiceComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
