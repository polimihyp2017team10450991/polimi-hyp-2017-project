import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AllServicesComponent } from './all-services.component';
import { ServiceComponent } from './service.component';
import { routing } from './service.router';
import { ServiceService } from './service.service';
import {Ng2FittextModule} from "ng2-fittext/ng2fittext";


@NgModule({
  imports: [
    CommonModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    Ng2FittextModule
  ],
  declarations: [
    ServiceComponent,
    AllServicesComponent
  ],
  providers: [ServiceService],
  bootstrap: [
    ServiceComponent
  ]
})
export class ServiceModule {}
