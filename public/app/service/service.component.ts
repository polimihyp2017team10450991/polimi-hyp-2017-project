import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { ServiceService } from './service.service';
import { Service } from '../shared/models/service';
import { Http, Headers } from '@angular/http';
import { ActivatedRoute, Params } from '@angular/router';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-service',
  styleUrls: ['./service.component.css'],
  templateUrl: './service.component.html'
})

export class ServiceComponent implements OnInit {
  id: number;
  private sub: any;

  previousService: Service;
  service: Service;
  nextService: Service;

  constructor(
    private route: ActivatedRoute,
    private serviceService: ServiceService) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];

      this.serviceService.getService(this.id - 1).subscribe(service => this.previousService = service[0]);
      this.serviceService.getService(this.id).subscribe(service => this.service = service[0]);
      this.serviceService.getService(this.id + 1).subscribe(service => this.nextService = service[0]);
    });

  }
}
