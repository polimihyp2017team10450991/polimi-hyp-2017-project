import { Component, OnInit } from '@angular/core';
import { ServiceService } from './service.service';
import { LocationsService } from '../location/locations.service';
import { Service } from '../shared/models/service';
import { Location } from '../shared/models/location';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'all-services',
  templateUrl: './all-services.component.html',
  styleUrls: ['./all-services.component.css']
})

export class AllServicesComponent implements OnInit {
  services: Service[];
  servicesByLocation: Service[];
  idLocation: number;
  location: Location;

  constructor(
    private serviceService: ServiceService,
    private locationsService: LocationsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    if(this.router.url.includes('location')) {

      this.route.params.subscribe(params => {
        this.idLocation = +params['id'];
      });

      this.locationsService.getLocation(this.idLocation).subscribe(locations => this.location = locations[0]);
      this.serviceService.getServiceByLocation(this.idLocation).subscribe(locations => this.servicesByLocation = locations);
    } else {
      this.serviceService.getServices().subscribe(services => this.services = services.slice(0,5));
    }
  }

}
