import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Service } from '../shared/models/service';
import 'rxjs/add/operator/map';

@Injectable()
export class ServiceService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private baseUrl: string = 'api/service';

  constructor(private http : Http) { }

  /*getAll(): Promise<Doctor[]> {
    return this.http.get(this.baseUrl)
    .toPromise()
    .then(response =>
      //console.log(response.json() as Doctor[]);
      response.json() as Doctor[]);
    //.catch(this.handleError);
  }*/

  getServices(): Observable<Service[]> {
    return this.http
               .get(this.baseUrl)
               .map(response => response.json().map(obj => new Service(obj)));
  }

  getService(id: number): Observable<Service> {
    return this.http
                .get(`${this.baseUrl}/${id}`)
                .map(response => response.json().map(obj => new Service(obj)));
  }

  getServicesByArea(area_id: number): Observable<Service[]> {
    return this.http
                .get(`${this.baseUrl}/area/${area_id}`)
                .map(response => response.json().map(obj => new Service(obj)));
  }

  getServiceByLocation(location_id: number): Observable<Service[]>{
    return this.http
                .get(`${this.baseUrl}/location/${location_id}`)
                .map(response => response.json().map(obj => new Service(obj)));
  }
}
