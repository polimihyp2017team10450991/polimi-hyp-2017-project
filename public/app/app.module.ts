import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Ng2FittextModule} from "ng2-fittext/ng2fittext";

import { AppComponent } from './app.component';
import { AppRouting } from './app.router';
import { NavbarComponent } from './navbar/navbar.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ReservationComponent } from './reservation/reservation.component';
import { MissionComponent } from './who-we-are/mission.component';
import { HistoryComponent } from './who-we-are/history.component';

import { LocationsService } from './location/locations.service';
import { AreasService } from './area/areas.service';
import { ServiceService } from './service/service.service';
import { ReservationService } from './reservation/reservation.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    ReservationComponent,
    MissionComponent,
    HistoryComponent
  ],
  imports: [
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRouting,
    Ng2FittextModule
  ],
  providers: [
    LocationsService,
    AreasService,
    ServiceService,
    ReservationService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
