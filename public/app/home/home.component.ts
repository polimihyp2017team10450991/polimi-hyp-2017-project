import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service/service.service';
import { Service } from '../shared/models/service';
import {Ng2FittextModule} from "ng2-fittext/ng2fittext";

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    services: Service[];

  constructor(private serviceService: ServiceService) { }

  ngOnInit() {
    this.serviceService.getServices().subscribe(services => this.services = services.slice(0,5));
  }

}
