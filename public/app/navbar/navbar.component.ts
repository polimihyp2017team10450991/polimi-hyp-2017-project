import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { RouterModule, Router } from '@angular/router';


@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  back: boolean = true;

  constructor(private location: Location, private router : Router) {
    router.events.subscribe(event => {
      if(this.router.url == '/')
        this.back = false;
      else
        this.back = true;
    })
  }

  ngOnInit() {
  }

  public isCollapsed:boolean = true;

  goBack() {
    this.location.back();
  }
}
