import { Component, OnInit } from '@angular/core';
import { Location } from '../shared/models/location';
import { LocationsService } from '../location/locations.service';
import { Area } from '../shared/models/area';
import { AreasService } from '../area/areas.service';
import { Service } from '../shared/models/service';
import { ServiceService } from '../service/service.service';
import { ReservationService } from './reservation.service';
import { Reservation } from '../shared/models/reservation';


@Component({
  selector: 'reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  submitted: boolean = false;

  locations: Location[];
  areas: Area[];
  services: Service[];

  reservation: Reservation = new Reservation('','','female','','','','', 1, 1, 1, false);

  constructor(
    private locationsService: LocationsService,
    private areasService: AreasService,
    private servicesService: ServiceService,
    private reservationService: ReservationService) {}

  ngOnInit() {
    this.setData();
  }

  setService(area_id) {
    this.servicesService.getServicesByArea(area_id).subscribe(services => {
      this.services = services;
      this.reservation.service = services[0].id;
    });
  }

  sendEmail() {
    //TODO: save in db and validate form
    this.locationsService.getLocation(this.reservation.location).subscribe(locations => {
      this.reservation.location_address = locations[0].address;
      this.areasService.getArea(this.reservation.area).subscribe(areas => {
        this.reservation.area_name = areas[0].name;
        this.servicesService.getService(this.reservation.service).subscribe(services => {
          this.reservation.service_name = services[0].name;
          this.reservationService.sendEmail(this.reservation);
        });
      });
    });
    this.submitted = true;
  }

  setData() {
    this.locationsService.getLocations().subscribe(locations => this.locations = locations);
    this.areasService.getAreas().subscribe(areas => {
      this.areas = areas;
      this.servicesService.getServicesByArea(areas[0].id).subscribe(services => {
        this.services = services;
        this.reservation.service = services[0].id;
      });
    });
  }

  refresh(): void {
    this.reservation = new Reservation('','','female','','','','', 1, 1, 1, false);
    this.submitted = false;
    this.setData();
  }

}
