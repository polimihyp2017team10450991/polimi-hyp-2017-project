import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Reservation } from '../shared/models/reservation';
import 'rxjs/add/operator/map';

@Injectable()
export class ReservationService {

  private baseUrl: string = 'api/mail';

  constructor(private http : Http) { }

  sendEmail(reservation: Reservation) {
    console.log('Sending the email...');
    return this.http
               .post(this.baseUrl, reservation)
               .subscribe(
                 err => console.error(err),
                 () => console.log('Email sent')
               );
  }
}
