import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Area } from '../shared/models/area';
import { OpeningTime } from '../shared/models/opening_time';
import 'rxjs/add/operator/map';

@Injectable()
export class AreasService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private baseUrl: string = 'api/area';

  constructor(private http : Http) { }

  getAreas(): Observable<Area[]> {
    return this.http
               .get(this.baseUrl)
               .map(response => response.json().map(obj => new Area(obj)));
  }

  getArea(id: number): Observable<Area> {
    return this.http
                .get(`${this.baseUrl}/${id}`)
                .map(response => response.json().map(obj => new Area(obj)));
  }

}
