import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { LocationsComponent } from './location/locations.component';
import { ReservationComponent } from './reservation/reservation.component';
import { MissionComponent } from './who-we-are/mission.component';
import { HistoryComponent } from './who-we-are/history.component';

const routes: Route[] = [
  { loadChildren: 'app/doctor/doctors.module#DoctorsModule', path: 'doctor' },
  { loadChildren: 'app/location/locations.module#LocationsModule', path: 'location' },
  { loadChildren: 'app/service/service.module#ServiceModule', path: 'service' },
  { path: 'reservation', component: ReservationComponent },
  { path: 'mission', component: MissionComponent },
  { path: 'history', component: HistoryComponent },
  { path: '', component: HomeComponent}
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(
  routes,
  {
    useHash: true
  }
);
