import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AllLocationsComponent } from './all-locations/all-locations.component';
import { LocationComponent } from './location/location.component';
import { LocationsComponent } from './locations.component';
import { routing } from './locations.router';
import { LocationsService } from './locations.service';
import { ServiceService } from '../service/service.service';
import { SafePipe } from '../safe.pipe';
import {Ng2FittextModule} from "ng2-fittext/ng2fittext";


@NgModule({
  imports: [
    CommonModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    Ng2FittextModule
  ],
  declarations: [
    LocationsComponent,
    LocationComponent,
    AllLocationsComponent,
    SafePipe
  ],
  providers: [LocationsService],
  bootstrap: [
    LocationsComponent
  ]
})
export class LocationsModule {}
