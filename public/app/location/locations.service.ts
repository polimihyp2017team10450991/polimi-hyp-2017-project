import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Location } from '../shared/models/location';
import { OpeningTime } from '../shared/models/opening_time';
import 'rxjs/add/operator/map';

@Injectable()
export class LocationsService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private baseUrl: string = 'api/location';

  constructor(private http : Http) { }

  getLocations(): Observable<Location[]> {
    return this.http
               .get(this.baseUrl)
               .map(response => response.json().map(obj => new Location(obj)));
  }

  getLocationsByService(id: number): Observable<Location[]> {
    return this.http
               .get(`${this.baseUrl}/service/${id}`)
               .map(response => response.json().map(obj => new Location(obj)));
  }

  getLocation(id: number): Observable<Location> {
    return this.http
                .get(`${this.baseUrl}/${id}`)
                .map(response => response.json().map(obj => new Location(obj)));
  }

  getOpeningTimes(id: number): Observable<OpeningTime[]> {
    return this.http
                .get(`${this.baseUrl}/times/${id}`)
                .map(response => response.json().map(obj => new OpeningTime(obj)));
  }

}
