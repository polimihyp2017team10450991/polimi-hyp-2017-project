import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Location } from '../shared/models/location';

@Component({
  selector: 'locations',
  styleUrls: ['./location/location.component.css'],
  template: '<router-outlet></router-outlet>'
})
export class LocationsComponent{

}
