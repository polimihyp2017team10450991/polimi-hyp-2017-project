import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../locations.service';
import { ServiceService } from '../../service/service.service';
import { Location } from '../../shared/models/location';
import { Service } from '../../shared/models/service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'all-locations',
  templateUrl: './all-locations.component.html',
  styleUrls: ['./all-locations.component.css']
})
export class AllLocationsComponent implements OnInit {
  locations: Location[];
  url: string;
  doctor = false;
  locationsByService: Location[];
  idService: number;
  service: Service;

  constructor(
    private router: Router,
    private locationsService: LocationsService,
    private serviceService: ServiceService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    if(this.router.url.includes('doctor')) {
      this.doctor = true;
      this.url = '/doctor/location';
      this.locationsService.getLocations().subscribe(locations => this.locations = locations);
    }
    else if(this.router.url.includes('service')){
      this.route.params.subscribe(params => {
        this.idService = +params['id'];
      });

      this.serviceService.getService(this.idService).subscribe(services => this.service = services[0]);
      this.locationsService.getLocationsByService(this.idService).subscribe(locations => this.locationsByService = locations);

    }else{
      this.url = '/location';
      this.locationsService.getLocations().subscribe(locations => this.locations = locations);
    }
  }
}
