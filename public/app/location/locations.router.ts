import { RouterModule, Route} from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LocationComponent } from './location/location.component';
import { LocationsComponent } from './locations.component';
import { AllLocationsComponent } from './all-locations/all-locations.component';

const routes: Route[] = [

  { path: '',  component: LocationsComponent, children: [
    { path: 'service/:id', component: AllLocationsComponent },
    { path: ':id', component: LocationComponent },
    { path: '', component: AllLocationsComponent },

    ]}


];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
