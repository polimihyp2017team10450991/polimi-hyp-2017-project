import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../locations.service';
import { Location } from '../../shared/models/location';
import { OpeningTime } from '../../shared/models/opening_time';
import { ActivatedRoute, Params } from '@angular/router';
import { DomSanitizer,SafeResourceUrl } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit, PipeTransform {
  id: number;
  location: Location;
  opening_times: OpeningTime[];
  url;

  constructor(
    private route: ActivatedRoute,
    private locationsService: LocationsService,
    private sanitizer: DomSanitizer) { }

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
       this.id = +params['id'];
       this.locationsService.getOpeningTimes(this.id)
       .subscribe(opening_times => this.opening_times = opening_times);
       this.locationsService.getLocation(this.id).subscribe(location => {
         this.location = location[0];
         this.url = "https://www.google.com/maps/embed/v1/place?key=AIzaSyBCBQZJ-pxmBkvbBvkMo_5HuKnMmuGilEg&q="+this.location.address;
       });
    });
  }

}
