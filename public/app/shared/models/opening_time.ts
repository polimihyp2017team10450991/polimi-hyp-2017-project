export class OpeningTime {
  location: number;
  weekday: string;
  start_time: string;
  end_time: string;

  constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
