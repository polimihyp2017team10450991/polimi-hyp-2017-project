export class Area {
  id: number;
  name: string;
  responsible: number;

  constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
