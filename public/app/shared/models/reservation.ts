import { Location } from './location';
import { Area } from './area';
import { Service } from './service';

export class Reservation {
  constructor(
    public name: string,
    public surname: string,
    public gender: string,
    public birthdate: string,
    public id_card: string,
    public email: string,
    public phone: string,
    public location: number,
    public area: number,
    public service: number,
    public terms: boolean,
    public comment?: string,
    public location_address?: string,
    public area_name?: string,
    public service_name?: string
  ) { }
}
