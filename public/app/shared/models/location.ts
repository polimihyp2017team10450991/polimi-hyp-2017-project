export class Location {
  id: number;
  address: string;
  email: string;
  phone: string;
  description: string;
  imageSrc: string;

  constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
