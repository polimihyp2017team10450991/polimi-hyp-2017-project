export class Doctor {
  id: number;
  name: string;
  surname: string;
  email: string;
  degree: string;
  phone: string;
  qualification: string;
  location: number;
  profile: string;
  curriculumVitae: string;
  treatments: string;
  service: number;
  responsibleOfService: number;
  responsibleOfArea: number;
  constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
