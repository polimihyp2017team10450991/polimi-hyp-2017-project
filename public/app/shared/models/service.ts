export class Service {
  id: number;
  name: string;
  responsibleOfService: number;
  areaOfService: number;
  description: string;
  imageSrc:string;
  constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
