import { RouterModule, Route} from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { DoctorComponent } from './doctor/doctor.component';
import { DoctorsComponent } from './doctors.component';
import { ProfileComponent } from './doctor/profile.component';
import { TreatmentsComponent } from './doctor/treatments.component';
import { CurriculumComponent } from './doctor/curriculum.component';
import { AllDoctorsComponent } from './all-doctors/all-doctors.component';
import { AllLocationsComponent } from '../location/all-locations/all-locations.component';
import { LocationComponent } from '../location/location/location.component';

const routes: Route[] = [

  { path: '',  component: DoctorsComponent, children: [
    {path: 'location', component: AllLocationsComponent},
    {path: 'location/:id', component: AllDoctorsComponent},
    {path: 'location/:location_id/:id', component: DoctorComponent,
      children: [
        { path: 'profile', component: ProfileComponent },
        { path: 'treatments', component: TreatmentsComponent },
        { path: 'curriculum', component: CurriculumComponent },
        { path: '', redirectTo: 'profile', pathMatch: 'full' }
      ]
    },
    {path: 'location', component: AllDoctorsComponent},
    {path: 'location/:id', component: DoctorComponent},

    {path: 'service/:service_id', component: AllDoctorsComponent},
    {path: 'service/:service_id/:id', component: DoctorComponent,
    children: [
        { path: 'profile', component: ProfileComponent },
        { path: 'treatments', component: TreatmentsComponent },
        { path: 'curriculum', component: CurriculumComponent },
        { path: '', redirectTo: 'profile', pathMatch: 'full' }
      ]
    },

    {path: 'all/:id', component: DoctorComponent,
      children: [
        { path: 'profile', component: ProfileComponent },
        { path: 'treatments', component: TreatmentsComponent },
        { path: 'curriculum', component: CurriculumComponent },
        { path: '', redirectTo: 'profile', pathMatch: 'full' }
      ]
    },
    {path: ':id', component: DoctorComponent,
      children: [
        { path: 'profile', component: ProfileComponent },
        { path: 'treatments', component: TreatmentsComponent },
        { path: 'curriculum', component: CurriculumComponent },
        { path: '', redirectTo: 'profile', pathMatch: 'full' }
      ]
    },
    {path: '', component: AllDoctorsComponent},

    ]}


];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
