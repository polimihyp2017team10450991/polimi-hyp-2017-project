import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { DoctorsService } from '../doctors.service';
import { Doctor } from '../../shared/models/doctor';
import { Http, Headers } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'treatments',
  styleUrls: ['./doctor.component.css'],
  template: '<div [innerHTML]="doctor?.curriculumVitae"></div>'
})
export class CurriculumComponent implements OnInit {
  id: number;
  location_id: number;
  url: string;

  doctors : Doctor[];

  previousDoctor: Doctor;
  doctor: Doctor;
  nextDoctor: Doctor;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private doctorsService: DoctorsService) { }

  ngOnInit(): void {
    this.route.parent.params.subscribe(params => {
       this.id = +params['id'];
       this.url = "/doctor";

       if(this.router.url.includes('location')) {
         this.url+="/location";
         this.doctorsService.getDoctor(this.id).subscribe(doctor => {
           this.location_id = doctor[0].location;
           this.doctorsService.getDoctorByLocation(this.location_id).subscribe(doctors => this.setDoctors(doctors));
         });
       }
       else {
         this.doctorsService.getDoctors().subscribe(doctors => this.setDoctors(doctors));
       }
    });

  }

  private setDoctors(doctors: Doctor[]): void {
    let index = doctors.findIndex(doctor => doctor.id == this.id);
    this.previousDoctor = doctors[index-1];
    this.doctor = doctors[index];
    this.nextDoctor = doctors[index+1];
  }

  private removeFocus(event)  {
    event.target.blur();
  }
}
