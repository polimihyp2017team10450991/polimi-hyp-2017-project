import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { DoctorsService } from '../doctors.service';
import { Doctor } from '../../shared/models/doctor';
import { Http, Headers } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LocationsService } from '../../location/locations.service';
import { ServiceService } from '../../service/service.service';
import { Location } from '../../shared/models/location';
import { Service } from '../../shared/models/service';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'doctor',
  styleUrls: ['./doctor.component.css'],
  templateUrl: './doctor.component.html'
})
export class DoctorComponent implements OnInit {
  id: number;
  location_id: number;
  location_address: string;
  url: string;
  service_id: number;
  doctors : Doctor[];
  service: Service;

  previousDoctor: Doctor;
  doctor: Doctor;
  nextDoctor: Doctor;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private doctorsService: DoctorsService,
    private locationsService: LocationsService,
    private serviceService: ServiceService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
       this.id = +params['id'];
       this.url = "/doctor";

       if(this.router.url.includes('location')) {//asking for doctor by location

         this.location_id = +params['location_id'];
         this.url+="/location/"+this.location_id;
         //this.doctorsService.getDoctor(this.id).subscribe(doctor => {
           //this.location_id = doctor[0].location;
           this.locationsService.getLocation(this.location_id).subscribe(locations => this.location_address = locations[0].address);
           this.doctorsService.getDoctorByLocation(this.location_id).subscribe(doctors => this.setDoctors(doctors));
         //});
       } else if( this.router.url.includes('service')){//asking for doctor by service
         this.service_id = +params['service_id'];//setting right url for next and previous button in doctorComponent
         this.url+="/service/"+this.service_id;

         this.serviceService.getService(this.service_id).subscribe(service => this.service = service[0]);
         this.doctorsService.getDoctorsByService(this.service_id).subscribe(doctors => this.setDoctors(doctors));

       } else if( this.router.url.includes('all')) {//asking for doctor
         this.url+="/all/";
         this.doctorsService.getDoctors().subscribe(doctors => this.setDoctors(doctors));
       }
       else {
         this.doctorsService.getDoctor(this.id).subscribe(doctors => this.doctor = doctors[0]);
       }
    });
  }

  private setDoctors(doctors: Doctor[]): void {
    let index = doctors.findIndex(doctor => doctor.id == this.id);
    this.previousDoctor = doctors[index-1];
    this.doctor = doctors[index];
    this.nextDoctor = doctors[index+1];
  }

  private removeFocus(event)  {
    event.target.blur();
  }
}
