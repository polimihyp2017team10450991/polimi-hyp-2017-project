import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AllDoctorsComponent } from './all-doctors/all-doctors.component';
import { DoctorComponent } from './doctor/doctor.component';
import { ProfileComponent } from './doctor/profile.component';
import { TreatmentsComponent } from './doctor/treatments.component';
import { CurriculumComponent } from './doctor/curriculum.component';
import { DoctorsComponent } from './doctors.component';
import { routing } from './doctors.router';
import { DoctorsService } from './doctors.service';
import {Ng2FittextModule} from "ng2-fittext/ng2fittext";


@NgModule({
  imports: [
    CommonModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    Ng2FittextModule,
  ],
  declarations: [
    DoctorsComponent,
    DoctorComponent,
    ProfileComponent,
    TreatmentsComponent,
    CurriculumComponent,
    AllDoctorsComponent
  ],
  providers: [DoctorsService],
  bootstrap: [
    DoctorsComponent
  ]
})
export class DoctorsModule {}
