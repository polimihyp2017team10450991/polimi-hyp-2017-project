import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Doctor } from '../shared/models/doctor';
import 'rxjs/add/operator/map';

@Injectable()
export class DoctorsService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private baseUrl: string = 'api/doctor';

  constructor(private http : Http) { }

  getDoctors(): Observable<Doctor[]> {
    return this.http
               .get(this.baseUrl)
               .map(response => response.json().map(obj => new Doctor(obj)));
  }

  getDoctor(id: number): Observable<Doctor> {
    return this.http
                .get(`${this.baseUrl}/${id}`)
                .map(response => response.json().map(obj => new Doctor(obj)));
  }

  getDoctorByLocation(id: number): Observable<Doctor[]> {
    return this.http
                .get(`${this.baseUrl}/location/${id}`)
                .map(response => response.json().map(obj => new Doctor(obj)));
  }

  getDoctorsByService(id: number): Observable<Doctor[]> {
    return this.http
                .get(`${this.baseUrl}/service/${id}`)
                .map(response => response.json().map(obj => new Doctor(obj)));
  }
}
