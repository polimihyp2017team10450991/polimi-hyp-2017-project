import { Component, OnInit } from '@angular/core';
import { DoctorsService } from '../doctors.service';
import { Doctor } from '../../shared/models/doctor';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LocationsService } from '../../location/locations.service';
import { ServiceService } from '../../service/service.service';
import { Location } from '../../shared/models/location';
import { Service } from '../../shared/models/service';

@Component({
  selector: 'all-doctors',
  templateUrl: './all-doctors.component.html',
  styleUrls: ['./all-doctors.component.css']
})
export class AllDoctorsComponent implements OnInit {
  doctors: Doctor[];
  location: Location;
  id: number;
  locations: Location[];
  doctorsByService: Doctor[];
  idService: number;
  service: Service;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private doctorsService: DoctorsService,
    private locationsService: LocationsService,
    private serviceService: ServiceService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       this.id = +params['id'];

       if(this.router.url.includes('location')) {
         this.locationsService.getLocation(this.id).subscribe(locations => this.location = locations[0])
         this.doctorsService.getDoctorByLocation(this.id).subscribe(doctors => this.doctors = doctors);
       }else if( this.router.url.includes('service') ){

         this.route.params.subscribe(params => {
           this.idService = +params['service_id'];
         });

         this.serviceService.getService(this.idService).subscribe(service => this.service = service[0]);
         this.doctorsService.getDoctorsByService(this.idService).subscribe(doctors => this.doctorsByService = doctors);

       } else {
         this.doctorsService.getDoctors().subscribe(doctors => this.doctors = doctors);
       }
    });

  }

}
