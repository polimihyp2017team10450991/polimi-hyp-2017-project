import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Doctor } from '../shared/models/doctor';

@Component({
  selector: 'doctors',
  styleUrls: ['./doctor/doctor.component.css'],
  template: '<router-outlet></router-outlet>'
})
export class DoctorsComponent{

}
